# Python N vizualization

Some tasks and little projects from MEPhI courses.
- data_analysis_course/ (data analysis a.k.a. python programming)
- vizualization_course/ (scientific vizualization)
- ml_course/ (regression, classification, clustering)
- cv_course/ (CV)  
A few internship assessment tasks are also provided (analytics_test).
